﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectStructure.Common.DTOapp.Update
{
    public class TasksUpdateDTO
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        private string name;
        public string Description { get; set; }
        public int State { get; set; }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value.Length < 1)
                    throw new ArgumentException();
                name = value;
            }
        }
    }
}
