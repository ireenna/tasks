﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectStructure.Common.DTOapp.Create
{
    public class TeamCreateDTO
    {
        public string Name { get; set; }
    }
    public class TeamCreateDTOValidator : AbstractValidator<TeamCreateDTO>
    {
        public TeamCreateDTOValidator()
        {
            RuleFor(a => a.Name).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
        }
    }
}
