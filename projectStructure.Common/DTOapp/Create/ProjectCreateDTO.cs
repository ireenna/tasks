﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectStructure.Common.DTOapp
{
    public class ProjectCreateDTO
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public DateTime Deadline { get; set; }
    }
    public class ProjectCreateDTOValidator : AbstractValidator<ProjectCreateDTO>
    {
        public ProjectCreateDTOValidator()
        {
            RuleFor(a => a.Name).NotNull().NotEmpty().MinimumLength(2);
            RuleFor(a => a.AuthorId).NotNull().NotEmpty();
            RuleFor(a => a.Deadline).NotNull().NotEmpty().GreaterThan(DateTime.Now);
        }
    }
}
