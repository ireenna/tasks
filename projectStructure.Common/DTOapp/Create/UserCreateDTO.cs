﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace projectStructure.Common.DTOapp.Create
{
    public class UserCreateDTO
    {
        public int? TeamId { get; set; }
        public string FirstName {get;set;}
        public string LastName
        {get;set;}
        public string Email {get;set; }
        public DateTime BirthDay { get;set; }
    }
        public class UserCreateDTOValidator : AbstractValidator<UserCreateDTO>
        {
            public UserCreateDTOValidator()
            {
                RuleFor(a => a.FirstName).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
                RuleFor(a => a.LastName).NotNull().NotEmpty().MinimumLength(2).MaximumLength(20);
                RuleFor(a => a.Email).NotNull().NotEmpty().Matches(@"^[^@\s]+@[^@\s]+\.[^@\s]+$", RegexOptions.IgnoreCase);
                RuleFor(a => a.BirthDay).NotNull().NotEmpty().LessThan(DateTime.Now).GreaterThan(DateTime.Now.AddYears(-120));
        }
    }
    
}
