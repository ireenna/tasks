﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using projectStructure.BLL.Services;
using projectStructure.Common.DTOapp.Create;
using projectStructure.DAL;

namespace projectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly LinqService _linqService;
        private readonly TeamService _teamService;
        public TeamsController(TeamService teamService, LinqService linqService)
        {
            _teamService = teamService;
            _linqService = linqService;
        }
        [HttpGet]
        public async Task<IEnumerable<Team>>  Get()
        {
            return await _teamService.GetAllTeams();
        }
        [HttpGet("{id}")]
        public async Task<Team>  Get([FromRoute] int id)
        {
            return await _teamService.GetTeam(id);
        }
        [HttpPost]
        public async Task<ActionResult<Team>> Create([FromBody] TeamCreateDTO proj)
        {
            try
            {
                var project = await _teamService.Create(proj);
                return Created("https://localhost:44326/api/Teams/" + project.Id,project);
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Team>> Update([FromBody] TeamCreateDTO proj, [FromRoute] int id)
        {
            try
            {
                var team = await _teamService.Update(proj, id);
                return Ok(team);
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Team>>  Delete([FromRoute] int id)
        {
            try
            {
                var team = await _teamService.Delete(id);
                return Ok(team);
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpGet("sorted")]
        public async Task<string> GetSortedUserTeams()
        {
            return JsonConvert.SerializeObject(await _linqService.GetSortedUsersTeams(), Formatting.Indented);
        }

    }
}
