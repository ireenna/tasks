﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using projectStructure.BLL.ModelsInfo;
using projectStructure.BLL.Services;
using projectStructure.Common.DTOapp;
using projectStructure.DAL;

namespace projectStructure.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;
        private readonly LinqService _linqService;

        public ProjectsController(ProjectService projectService, LinqService linqService)
        {
            _projectService = projectService;
            _linqService = linqService;
        }

        [HttpGet]
        public async Task<IEnumerable<Project>> Get()
        {
            return await _projectService.GetAllProjects();
        }
        [HttpGet("{id}")]
        public async Task<Project> Get([FromRoute] int id)
        {
            return await _projectService.GetProject(id);
        }
        [HttpPost]
        public async Task<ActionResult<Project>> Create([FromBody] ProjectCreateDTO proj)
        {
            try
            {
                Project project = await _projectService.Create(proj);
                return Created("https://localhost:44326/api/Projects/"+project.Id, project);
            }
            catch
            {
                return BadRequest();
            }
            
           
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Project>> Update([FromBody] ProjectUpdateDTO proj, [FromRoute] int id)
        {
            try
            {
                Project project = await _projectService.Update(proj, id);
                return Ok(project);
            }
            catch
            {
                return BadRequest();
            }
            

        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Project>> Delete([FromRoute] int id)
        {
            try
            {
                Project project = await _projectService.Delete(id);
                return Ok(project);
            }
            catch
            {
                return BadRequest();
            }
        }
        //[HttpGet("info")]
        //public async Task<List<ProjectsInfo>> GetProjectsInfo()
        //{
        //    return await _linqService.GetProjectsInfo();
        //}
    }
}
