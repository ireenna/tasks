﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using projectStructure.BLL.ModelsInfo;
using projectStructure.BLL.Services;
using projectStructure.Common.DTOapp.Create;
using projectStructure.Common.DTOapp.Update;
using projectStructure.DAL;

namespace projectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly LinqService _linqService;
        private readonly UserService _userService;
        public UsersController(UserService userService, LinqService linqService)
        {
            _userService = userService;
            _linqService = linqService;
        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await _userService.GetAllUsers();
        }
        [HttpGet("{id}")]
        public async Task<User> Get([FromRoute] int id)
        {
            return await _userService.GetUser(id);
        }
        [HttpPost]
        public async Task<ActionResult<User>> Create([FromBody] UserCreateDTO proj)
        {
            try
            {
                var user = await _userService.Create(proj);
                return Created("https://localhost:44326/api/Users/" + user.Id, user);
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<User>>  Update([FromBody] UserUpdateDTO proj, [FromRoute] int id)
        {
            try
            {
                return Ok(await _userService.Update(proj, id));
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>>  Delete([FromRoute] int id)
        {
            try
            {
                var user = await _userService.Delete(id);
                return Ok(user);
            }
            catch
            {
                return BadRequest();

            }
        }

        [HttpGet("{id}/project/alltasks")]
        public async Task<Dictionary<Project, int>> GetQuantityOfUserTasks([FromRoute] int id)
        {
            return await _linqService.GetQuantityOfUserTasks(id);
        }
        [HttpGet("{id}/tasks/filtered")]
        public async Task<List<Tasks>> GetUserTasks([FromRoute] int id)
        {
            return await _linqService.GetUserTasks(id);
        }
        [HttpGet("{id}/tasks/finished")]
        public async Task<string> GetUserFinishedTasks([FromRoute] int id)
        {
            return JsonConvert.SerializeObject(await _linqService.GetUserFinishedTasks(id), Formatting.Indented);
        }
        [HttpGet("{id}/tasks/unfinished")]
        public async Task<List<Tasks>> GetUserUnfinishedTasks([FromRoute] int id)
        {
            return await _linqService.GetUserUnfinishedTasks(id);
        }
        [HttpGet("tasks/sorted")]
        public async Task<List<IGrouping<User, Tasks>>> GetSortedUsersWithTasks()
        {
            return await _linqService.GetSortedUsersWithTasks();
        }
        [HttpGet("{id}/tasks/info")]
        public async Task<UserTaskInfo> GetUserTasksInfo([FromRoute] int id)
        {
            return await _linqService.GetUserTasksInfo(id);
        }
    }
}
