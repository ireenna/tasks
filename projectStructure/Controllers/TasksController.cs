﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using projectStructure.BLL.Services;
using projectStructure.Common.DTOapp.Create;
using projectStructure.Common.DTOapp.Update;
using projectStructure.DAL;

namespace projectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly TasksService _tasksService;
        public TasksController(TasksService teamService)
        {
            _tasksService = teamService;
        }
        [HttpGet]
        public async Task<IEnumerable<Tasks>> Get()
        {
            return await _tasksService.GetAllTasks();
        }
        [HttpGet("{id}")]
        public async Task<Tasks> Get([FromRoute] int id)
        {
            return await _tasksService.GetTask(id);
        }
        [HttpPost]
        public async Task<ActionResult<Tasks>> Create([FromBody] TasksCreateDTO proj)
        {
            try
            {
                var task = await _tasksService.Create(proj);
                return Created("https://localhost:44326/api/Tasks/" + task.Id, task);
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Tasks>> Update([FromBody] TasksUpdateDTO proj, [FromRoute] int id)
        {
            try
            {
                var task = await _tasksService.Update(proj,id);
                return Ok(task);
            }
            catch
            {
                return BadRequest();
            }
            
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            try
            {
                var task = await _tasksService.Delete(id);
                return Ok(task);
            }
            catch
            {
                return BadRequest();
            }
        }

    }
}
