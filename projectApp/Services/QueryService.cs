﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using projectStructureApp.ModelsApp.Update;

namespace projectStructureApp.Services
{
    public sealed class QueryService
    {
        private readonly AppService app;
        public QueryService()
        {
            app = new AppService();
        }

        public Task<int> MarkRandomTaskWithDelay(int delay = 2000)
        {
            TaskCompletionSource<int> tsc = new TaskCompletionSource<int>();

            System.Timers.Timer timer = new System.Timers.Timer(delay);
            Random random = new Random();
            ElapsedEventHandler handler = null;
            handler = async (sender, eventArgs) =>
            {
                try
                {
                    timer.Elapsed -= handler;

                    var allTasks = await app.GetAllTasks();
                    var randomTask = allTasks[random.Next(0, allTasks.Count)];
                    if (randomTask.State == ModelsDTOapp.TaskStateDTOapp.Done)
                    {
                        tsc.SetException(new Exception($"Error. This task {randomTask.Id} {randomTask.Name} has already been marked as finished"));
                    }
                    else
                    {
                        var updatedTask = new TasksUpdateApp()
                        {
                            State = ModelsDTOapp.TaskStateDTOapp.Done,
                            FinishedAt = DateTime.Now,
                            Description = randomTask.Description,
                            Name = randomTask.Name,
                            PerformerId = randomTask.PerformerId,
                            ProjectId = randomTask.ProjectId
                        };
                        await app.UpdateTask(updatedTask, randomTask.Id);
                        tsc.SetResult(randomTask.Id);
                    }
                }
                catch (Exception ex)
                {
                    tsc.SetException(ex);
                }
                finally
                {
                    timer.Stop();
                    timer.Dispose();
                }

            };
            timer.Elapsed += handler;
            timer.Start();

            return tsc.Task;
        }


        //public void MarkRandomTaskWithDelay()
        //{
        //    BackgroundWorker backgroundWorker = new BackgroundWorker
        //    {
        //        WorkerReportsProgress = true,
        //        WorkerSupportsCancellation = true
        //    };
        //    backgroundWorker.DoWork += BackgroundWorkerOnDoWork;
        //    backgroundWorker.ProgressChanged += BackgroundWorkerOnProgressChanged;
        //}

        //private void BackgroundWorkerOnProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    object userObject = e.UserState;
        //    int percentage = e.ProgressPercentage;
        //    //if(e.Err)
        //}

        //private void BackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        //{
        //    BackgroundWorker worker = (BackgroundWorker)sender;
        //    while (!worker.CancellationPending)
        //    {
        //        //Do your stuff here
        //        worker.ReportProgress(0, "AN OBJECT TO PASS TO THE UI-THREAD");
        //    }
        //}


        //private CancellationTokenSource _tokenSource;
        //private async void RunInfinitiveLoop(int delay)
        //{
        //    _tokenSource = new CancellationTokenSource();
        //    await MarkRandomTaskWithDelay(delay, _tokenSource.Token);
        //}
        //private void StopInfinitiveLoop()
        //{
        //    _tokenSource.Cancel();
        //}
        //public Task<int> MarkRandomTaskWithDelay(int delayInMS, CancellationToken token)
        //{
        //    return Task.Run(async () => {
        //        while (true)
        //        {
        //            var tcs = new TaskCompletionSource<int>();
        //            var backgroundWorker = new BackgroundWorker();
        //            System.Timers.Timer timer = new System.Timers.Timer(delayInMS);

        //            backgroundWorker.DoWork += (o, e) =>
        //            {

        //            };
        //            backgroundWorker.RunWorkerCompleted += (o, e) =>
        //            {
        //                if (e.Error != null)
        //                {
        //                    tcs.SetException(e.Error);
        //                }
        //                else
        //                {
        //                    tcs.SetResult(1);
        //                }
        //            };
        //            if (token.IsCancellationRequested)
        //            {
        //                break;
        //            }
        //            return tcs.Task;
        //        };
        //    }, token);


        //async void eventHandler(object o, ElapsedEventArgs args)
        //{
        //    timer.Elapsed -= eventHandler;
        //    var allTasks = await app.GetAllTasks();
        //    Random r = new Random();
        //    var randomTask = allTasks[r.Next(0, allTasks.Count)];
        //    if (randomTask.State == ModelsDTOapp.TaskStateDTOapp.Done)
        //    {
        //        tcs.SetException(new Exception($"Error. This task {randomTask.Id} {randomTask.Name} has already been marked as finished"));
        //    }
        //    else
        //    {
        //        var updatedTask = new TasksUpdateApp()
        //        {
        //            State = ModelsDTOapp.TaskStateDTOapp.Done,
        //            FinishedAt = DateTime.Now,
        //            Description = randomTask.Description,
        //            Name = randomTask.Name,
        //            PerformerId = randomTask.PerformerId,
        //            ProjectId = randomTask.ProjectId
        //        };
        //        await app.UpdateTask(updatedTask, randomTask.Id);
        //        tcs.SetResult(randomTask.Id);
        //    }

        //}
        //timer.Start();
        //    timer.Elapsed += eventHandler;
        //    timer.Enabled = true;
        //    timer.AutoReset = true;
        //return tcs.Task;
        //}
    }
}
