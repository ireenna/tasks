﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using projectStructure.Common.DTOapp;
using projectStructure.DAL;
using projectStructure.DAL.Repositories;

namespace projectStructure.BLL.Services
{
    public sealed class ProjectService : BaseService
    {
        private readonly IRepository<Project> _projRepo;
        private readonly IRepository<Team> _teamRepo;
        private readonly IRepository<User> _userRepo;
        public ProjectService(IMapper mapper, IRepository<Project> projRepo, IRepository<Team> teamRepo, IRepository<User> userRepo) : base(mapper) {
            _projRepo = projRepo;
            _teamRepo = teamRepo;
            _userRepo = userRepo;
        }

        public async Task<IEnumerable<Project>> GetAllProjects()
        {
            return await _projRepo.Get(null, include: source => source.Include(x => x.Author).Include(x=>x.Team).Include(x=>x.Tasks));
        }
        public async Task<Project> GetProject(int id)
        {            
            return await _projRepo.GetByID(id);
        }
        public async Task<Project> Create(ProjectCreateDTO proj)
        {
            try
            {
                var project = new Project {
                    Author = await _userRepo.GetByID(proj.AuthorId),
                    CreatedAt = DateTime.Now,
                    Deadline = proj.Deadline,
                    Description = proj.Description,
                    Name = proj.Name,
                    Team = await _teamRepo.GetByID(proj.TeamId)
                };
                await _projRepo.Insert(project);
                return project;
            }
            catch
            {
                throw new Exception();
            }
            
        }
        public async Task<Project> Update(ProjectUpdateDTO proj, int id)
        {
            try
            {
                var oldProject = await _projRepo.GetByID(id);
                oldProject.Deadline = proj.Deadline;
                oldProject.Description = proj.Description;
                oldProject.Name = proj.Name;
                oldProject.Team = await _teamRepo.GetByID(proj.TeamId);
                oldProject.Author = await _userRepo.GetByID(proj.AuthorId);
                await _projRepo.Update(oldProject);
                return oldProject;
            }
            catch
            {
                throw new Exception();
            }

        }
        public async Task<Project> Delete(int id)
        {
            try
            {
                var oldProject = await _projRepo.GetByID(id);
                await _projRepo.Delete(id);

                return oldProject;
            }
            catch
            {
                throw new Exception();
            }

        }
    }
}
