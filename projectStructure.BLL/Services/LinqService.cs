﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Nito.AsyncEx;
using projectStructure.BLL.Interfaces;
using projectStructure.BLL.ModelsInfo;
using projectStructure.DAL;
using projectStructure.DAL.Repositories;

namespace projectStructure.BLL.Services
{
    public sealed class LinqService : BaseService//, ILinqService
    {
        private readonly IRepository<Project> _projRepo;
        private readonly IRepository<User> _userRepo;
        private readonly IRepository<Team> _teamRepo;
        private readonly IRepository<Tasks> _taskRepo;
        public LinqService(IMapper mapper, IRepository<Project> projRepo, IRepository<User> userRepo, IRepository<Team> teamRepo, IRepository<Tasks> taskRepo) : base(mapper)
        {
            _projRepo = projRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _taskRepo = taskRepo;
        }
        public async Task<Dictionary<Project, int>> GetQuantityOfUserTasks(int id)
        {
            var info = (await Task.WhenAll(_projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author))))
                .First()
                .Where(x => x.Author?.Id == id)
                .ToDictionary(x => x, x => x.Tasks.Count());

            if (info.Count == 0)
            {
                if (_userRepo.GetByID(id) is null)
                    throw new ArgumentOutOfRangeException();
            }

            return info;
        }
        public async Task<List<Tasks>> GetUserTasks(int id)
        {
            var projects = (await _projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author)))
                .SelectMany(x=>x.Tasks).Where(x => x.PerformerId == id && x.Name.Length < 45).ToList();

            if (projects.Count == 0)
            {
                if (_userRepo.GetByID(id) is null)
                    throw new ArgumentOutOfRangeException();
            }

            return projects;
        }
        public async Task<List<(int id, string name)>> GetUserFinishedTasks(int id)
        {
            var info = (await _projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author)))
                .SelectMany(p => p.Tasks)
                .Where(x => x.Performer.Id == id && x.FinishedAt <= DateTime.Now && x.FinishedAt >= new DateTime(2021, 1, 1))
                .Select(x => (id: x.Id, name: x.Name))
                .ToList();

            if (info.Count == 0)
            {
                if (_userRepo.GetByID(id) is null)
                    throw new ArgumentOutOfRangeException();
            }

            return info;
        }
        public async Task<List<(int id, string name, List<User> users)>> GetSortedUsersTeams()
        {
            var tasks = (await Task.WhenAll(_projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author))))
                .First()
                .Select(x => new
                {
                    Id = x.Team.Id,
                    Name = x.Team.Name,
                    Team = x.Team.CreatedAt,
                    Participants = x.Team.Participants
                }).Distinct()
                .Where(x => x.Participants.All(x => x.BirthDay <= DateTime.Now.AddYears(-10)))
                .Select(x => (x.Id, x.Name, x.Participants.OrderByDescending(p => p.RegisteredAt).ToList())).ToList();

            return tasks;
        }
        public async Task<List<IGrouping<User, Tasks>>> GetSortedUsersWithTasks()
        {
            var info = (await Task.WhenAll(_projRepo.Get(null, 
                include: 
                source=>source
                .Include(x=>x.Team).ThenInclude(x=>x.Participants)
                .Include(x=>x.Tasks).ThenInclude(x=>x.Performer)
                .Include(x=>x.Author))))
                .First()
                .SelectMany(p => p.Tasks)
                .OrderByDescending(t => t.Name.Length)
                .GroupBy(t => t.Performer)
                .OrderBy(x => x.Key.FirstName)
                .ToList();

            return info;
        }
        public async Task<UserTaskInfo> GetUserTasksInfo(int id)
        {
            var info = (await Task.WhenAll(_projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author))))
                .First()
                .SelectMany(p => p.Tasks)
                .Where(x => x.Performer.Id == id)
                .GroupBy(x => x.Performer)
                .Where(x => x.Key.Id == id)
                .Select(async x => new
                {
                    user = x.Key,
                    lastProject = (await Task.WhenAll(_projRepo.Get(null,
                    include:
                    source => source
                    .Include(x => x.Team).ThenInclude(x => x.Participants)
                    .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                    .Include(x => x.Author))))
                    .First().Where(p => p.Team.Participants.Contains(x.Key)).OrderByDescending(x => x.CreatedAt).FirstOrDefault(),
                    tasks = x
                }).Select(async x => new UserTaskInfo()
                {
                    User = (await x).user,
                    LastProject = (await x).lastProject,
                    LastProjectTasksCount = (await x).lastProject?.Tasks.Count() ?? 0,
                    RejectedTasks = (await x).tasks.Where(x => x.FinishedAt == null | (int)x.State == 2).Count(),
                    TheLongestTask = (await x).tasks.OrderByDescending(x => (x.FinishedAt ?? DateTime.Now) - x.CreatedAt).FirstOrDefault()
                }).FirstOrDefault();

            if (info is null)
            {
                if (_userRepo.GetByID(id) is null)
                    throw new ArgumentOutOfRangeException();
            }

            return await info;
        }
        public async Task<List<ProjectsInfo>> GetProjectsInfo()
        {
            var info = (await Task.WhenAll(_projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author))))
                .First()
                .Select(p => new ProjectsInfo()
                {
                    Project = p,
                    LongestTaskByDescr = p.Tasks?.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                    ShortestTaskByName = p.Tasks?.OrderBy(t => t.Name.Length).FirstOrDefault(),
                    UsersCount = p.Description.Length > 20 ^ p.Tasks?.Count < 3 ? p.Team.Participants?.Count : 0
                }).ToList();

            return info;
        }
        public async Task<List<Tasks>> GetUserUnfinishedTasks(int id)
        {
            var info = (await Task.WhenAll(_projRepo.Get(null,
                include:
                source => source
                .Include(x => x.Team).ThenInclude(x => x.Participants)
                .Include(x => x.Tasks).ThenInclude(x => x.Performer)
                .Include(x => x.Author))))
                .First()
                .SelectMany(p => p.Tasks).Where(t => t.Performer.Id == id && t.State != TaskState.Done).ToList();
            if (info.Count == 0)
            {
                if (_userRepo.GetByID(id) is null)
                    throw new ArgumentOutOfRangeException();
            }

            return info;
        }
    }
}
