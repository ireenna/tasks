﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using projectStructure.Common.DTOapp.Create;
using projectStructure.Common.DTOapp.Update;
using projectStructure.DAL;
using projectStructure.DAL.Repositories;

namespace projectStructure.BLL.Services
{
    public sealed class UserService : BaseService
    {
        private readonly IRepository<User> _userRepo;
        private readonly IRepository<Team> _teamRepo;
        public UserService(IMapper mapper, IRepository<User> userRepo, IRepository<Team> teamRepo) : base(mapper)
        {
            _userRepo = userRepo;
            _teamRepo = teamRepo;
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await _userRepo.Get(null);
        }
        public async Task<User>  GetUser(int id)
        {
            return await _userRepo.GetByID(id);
        }
        public async Task<User> Create(UserCreateDTO item)
        {
            try
            {
                var user = _mapper.Map<User>(item);
                await _userRepo.Insert(user);
                return user;
            }
            catch
            {
                throw new Exception();
            }
            

        }
        public async Task<User> Update(UserUpdateDTO proj, int id)
        {
            try
            {
                if (await _userRepo.GetByID(id) is null)
                    throw new ArgumentException();

                var oldUser = await _userRepo.GetByID(id);
                oldUser.BirthDay = proj.BirthDay;
                oldUser.Email = proj.Email;
                oldUser.FirstName = proj.FirstName;
                oldUser.LastName = proj.LastName;
                if (await _teamRepo.GetByID(proj.TeamId) is null)
                    throw new ArgumentException();
                oldUser.TeamId = proj.TeamId;
                await _userRepo.Update(oldUser);
                return oldUser;
            }
            catch
            {
                throw new Exception();
            }
            

        }
        public async Task<User>  Delete(int id)
        {
            try
            {
                var oldUser = await _userRepo.GetByID(id);
                await _userRepo.Delete(id);
                return oldUser;
            }
            catch
            {
                throw new Exception();
            }

        }
    }
}
