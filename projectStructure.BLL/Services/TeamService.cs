﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using projectStructure.Common.DTOapp.Create;
using projectStructure.DAL;
using projectStructure.DAL.Repositories;

namespace projectStructure.BLL.Services
{
    public sealed class TeamService : BaseService
    {
        private readonly IRepository<Team> _teamRepo;
        public TeamService(IMapper mapper, IRepository<Team> teamRepo) : base(mapper)
        {
            _teamRepo = teamRepo;
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            return await _teamRepo.Get(null, include: source=>source.Include(x=>x.Participants));
        }
        public async Task<Team> GetTeam(int id)
        {
            return await _teamRepo.GetByID(id);
        }
        public async Task<Team> Create(TeamCreateDTO item)
        {
            try
            {
                var team = _mapper.Map<Team>(item);
                await _teamRepo.Insert(team);
                return team;
            }
            catch
            {
                throw new Exception();
            }

        }
        public async Task<Team> Update(TeamCreateDTO proj, int id)
        {
            try
            {
                var oldTeam = await _teamRepo.GetByID(id);
                oldTeam.Name = proj.Name;
                await _teamRepo.Update(oldTeam);
                return oldTeam;
            }
            catch
            {
                throw new Exception();
            }

        }
        public async Task<Team> Delete(int id)
        {
            try
            {
                var oldTeam = await _teamRepo.GetByID(id);
                await _teamRepo.Delete(id);
                return oldTeam;
            }
            catch
            {
                throw new Exception();
            }

        }
    }
}
