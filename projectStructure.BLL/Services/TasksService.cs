﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using projectStructure.Common.DTOapp.Create;
using projectStructure.Common.DTOapp.Update;
using projectStructure.DAL;
using projectStructure.DAL.Repositories;

namespace projectStructure.BLL.Services
{
    public sealed class TasksService : BaseService
    {
        private readonly IRepository<Tasks> _tasksRepo;
        private readonly IRepository<User> _userRepo;
        public TasksService(IMapper mapper, IRepository<Tasks> tasksRepo, IRepository<User> userRepo) : base(mapper)
        {
            _tasksRepo = tasksRepo;
            _userRepo = userRepo;
        }

        public async Task<IEnumerable<Tasks>> GetAllTasks()
        {
            return await _tasksRepo.Get(null, include: source => source.Include(x => x.Performer));
        }
        public async Task<Tasks> GetTask(int id)
        {
            return await _tasksRepo.GetByID(id);
        }
        public async Task<Tasks> Create(TasksCreateDTO item)
        {
            try
            {
                var task = new Tasks() {
                    CreatedAt = DateTime.Now,
                    FinishedAt = null,
                    Description = item.Description,
                    Name = item.Name,
                    Performer = await _userRepo.GetByID(item.PerformerId),
                    ProjectId = item.ProjectId,
                    State = (TaskState)item.State
                };
                await _tasksRepo.Insert(task);
                return task;
            }
            catch
            {
                throw new Exception();
            }

        }
        public async Task<Tasks> Update(TasksUpdateDTO task, int id)
        {
            try
            {
                var oldTask = await _tasksRepo.GetByID(id);
                oldTask.Description = task.Description;
                oldTask.Name = task.Name;
                oldTask.Performer = await _userRepo.GetByID(task.PerformerId);
                oldTask.ProjectId = task.ProjectId;
                oldTask.State = (TaskState)task.State;
                if(oldTask.State == TaskState.Done)
                {
                    oldTask.FinishedAt = DateTime.Now;
                }
                await _tasksRepo.Update(oldTask);
                return oldTask;
            }
            catch
            {
                throw new Exception();
            }
                

        }
        public async Task<Tasks> Delete(int id)
        {
            try
            {
                var oldTask = await _tasksRepo.GetByID(id);
                await _tasksRepo.Delete(id);
                return oldTask;
            }
            catch
            {
                throw new Exception();
            }

        }
    }
}
