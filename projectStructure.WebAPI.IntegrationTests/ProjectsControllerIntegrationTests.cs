﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using projectStructureApp.ModelsDTOapp;
using Xunit;

namespace projectStructure.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests: IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        private HttpClient _client;
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CreateUser_WhenCorrectData_ThenCorrectResponse()
        {
            var proj = new Common.DTOapp.ProjectCreateDTO()
            {
                AuthorId = 1,
                Deadline = DateTime.Now.AddMonths(5),
                Description = "testDesc",
                Name = "testName",
                TeamId = 1
            };
            string jsonProj = JsonConvert.SerializeObject(proj);

            var response = await _client.PostAsync($"https://localhost:44326/api/projects", new StringContent(jsonProj, Encoding.UTF8, "application/json"));
            var strResp = await response.Content.ReadAsStringAsync();
            var responseOb = JsonConvert.DeserializeObject<ProjectDTOapp>(strResp);

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.Equal(proj.Name, responseOb.Name);
            Assert.Equal(proj.Deadline, responseOb.Deadline);
        }

        [Fact]
        public async Task CreateUser_WhenCorrectData_ThenCheckItExists()
        {
            var proj = new Common.DTOapp.ProjectCreateDTO()
            {
                AuthorId = 1,
                Deadline = DateTime.Now.AddMonths(5),
                Description = "testDesc",
                Name = "testName",
                TeamId = 1
            };
            string jsonProj = JsonConvert.SerializeObject(proj);

            var response = await _client.PostAsync($"https://localhost:44326/api/projects", new StringContent(jsonProj, Encoding.UTF8, "application/json"));
            var responseOb = JsonConvert.DeserializeObject<ProjectDTOapp>(await response.Content.ReadAsStringAsync());

            var getResponse = await _client.GetAsync($"https://localhost:44326/api/projects/" + responseOb.Id);
            var getResponseOb = JsonConvert.DeserializeObject<ProjectDTOapp>(await getResponse.Content.ReadAsStringAsync());

            Assert.Equal(JsonConvert.SerializeObject(responseOb), JsonConvert.SerializeObject(getResponseOb));
        }


        [Theory]
        [InlineData("{\"authorId\": 2,\"deadline\": \"2100-08-08\",\"description\": \"\",\"name\": \"\",\"teamId\": 1}")]
        [InlineData("{\"authorId\": 2,\"deadline\": \"2020-08-08\",\"description\": \"some text\",\"name\": \"name\",\"teamId\": 1}")]
        public async Task CreateProject_WhenWrongValidatedData_ThenThrowBadRequest(string jsonInString)
        {
            var client = _factory.CreateClient();
            var response = await client.PostAsync($"https://localhost:44326/api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData("{\"deadline\": \"2020-08-08\",\"description\": \"test\",\"name\": \"\",\"teamId\": 1}")]
        [InlineData("{\"authorId\": 0,\"deadline\": \"2020-08-08\",\"description\": \"test\",\"name\": \"N\",\"teamId\": 1}")]
        public async Task CreateProject_WhenWrongValidatedData_ThenResponseWithValidationError(string jsonInString)
        {
            var client = _factory.CreateClient();
            var response = await client.PostAsync($"https://localhost:44326/api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var responseOb = JsonConvert.DeserializeObject<JToken>(await response.Content.ReadAsStringAsync());
            var respArray = responseOb["errors"];

            Assert.Equal(3, respArray.Count());
            Assert.NotNull(responseOb.SelectToken("errors.Deadline"));
            Assert.NotNull(responseOb.SelectToken("errors.Name"));
            Assert.NotNull(responseOb.SelectToken("errors.AuthorId"));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

    }
}
